# GitLab Elasticsearch Analyzer Reports

Used as a place to coalesce reports generated by [GitLab Elasticsearch Analyzer!](https://gitlab.com/ehenley/es_analyzer).